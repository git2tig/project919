AT&T ECO COLLECT 2.0
MOP: ENABLE/DISABLE ECO COLLECT
DOCUMENT VERSION: 1.0 RELEASE DATE: MAY 10, 2017 INSTANCES: ALL INSTANCES AUTHOR: JIGNA PATEL
DOCUMENT VERSION: 1.0 RELEASE DATE: MAY 10, 2017 INSTANCES: ALL INSTANCES

TABLE OF CONTENTS

RELEASE DATE .................................................................................................................. i INSTANCES ....................................................................................................................... i RELEASE DATE .................................................................................................................. i INSTANCES ....................................................................................................................... i
REVISION HISTORY ....................................................................................... 4 REVIEW HISTORY.......................................................................................... 4 DOCUMENT CONVENTIONS ......................................................................... 5 ASK YOURSELF.............................................................................................. 8 OVERVIEW ................................................................................................... 9
Assumptions.................................................................................................................... 9
1 PREPARATION ..................................................................................... 11
1.1 Preparation Entrance........................................................................................ 11 1.2 Preparation Execution ...................................................................................... 12
Verify the Current MOP Version........................................................................................... 12
1.3 Preparation Exit ................................................................................................ 12
2 ENABLE ECO COLLECT ON ECO MANAGE SERVERS ............................. 13
2.1 Configuration Update Entrance........................................................................ 14 2.2 Configuration Update Execution for DMC Servers ........................................... 14
Backup the current ECO Manage DMC Application Server Configuration ........................... 14 Configure DMC to enable ECO Collect.................................................................................. 15 Perform a Rolling Restart of the DMC Application ............................................................... 16
2.3 Configuration Update Execution for SMS Servers............................................ 18
Backup the current ECO Manage SMS Application Configuration ....................................... 18 Configure SMS to enable ECO Collect................................................................................... 19 Perform an Rolling Restart of the SMS Application .............................................................. 20
2.4 Enable ECO Collect on ECO Manage Servers Phase Exit................................... 21
3 DISABLE ECO COLLECT ON ECO MANAGE SERVERS ............................. 22
3.1 Disable ECO Collect Entrance............................................................................ 23 3.2 Disable ECO Collect Execution .......................................................................... 23
Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

2

Use the appropriate rollback section from the appendix of the MOP-ATT_ECOCollect2.0Crawl_Walk_Run_Configuration mop to rollback to original configuration....................................... 23
Stop the DMC server by performing the below steps. ......................................................... 23
Re-install the original .config file on the dmc application as eco user. Choose `y' to overwrite the file when prompted to do so. ...................................................................................... 24
Start the DMC server by performing the below steps.......................................................... 24
Check the status of the DMC server by performing the below steps................................... 24
Ensure these steps are done each DMC server. ................................................................... 24
Stop the SMS server by performing the below steps. .......................................................... 25
Re-install the original .config file. Choose `y' to overwrite the file when prompted to do so. 25
Start the SMS server by performing the below steps........................................................... 25
Check the status of the SMS server by performing the below steps.................................... 25
EXECUTE:............................................................................................................................................. 25
Ensure that these steps are performed on each sms server. ............................................... 25
3.3 Disable ECO Collect on ECO Manage Servers Phase exit.................................. 26
APPENDIX A: NON PRODUCTION LAB CONFIGURATION REQUIREMENTS TO ENABLE ECO COLLECT................................................................................. 26

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

3

REVISION HISTORY

Version 1.0

Date May 10, 2017

REVIEW HISTORY

Reviewer

Date

Authors Jigna Patel
Title

Comments Initial Version

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

4

DOCUMENT CONVENTIONS
PLEASE READ THIS SECTION BEFORE PROCEEDING
The following new documentation conventions have been adapted for this new generation of ARRIS MOP documentation.
Note Callouts Note callouts provide information you may need to know prior to engaging in the following steps. Please read carefully.
Note: Provide out information you may need to know prior to engaging in the following steps. Please read carefully.
Best Practices Callouts Best Practice callouts indicate there may be corporate, industry or 3rd party vendor standards that you adhere to in this Section.
Red Hat, Oracle, and AT&T standard methods and best practices should also be followed when installing this software.

EXECUTE Steps Execute Steps detail specific statements to be entered in a terminal. Some steps may include variables that require the operator(s) to specific information.
EXECUTE:
Type these commands into your Linux console. Some commands may contain <variables> that you need to provide.
Expected Output
Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

5

Expected output the exact result expected of an executed command. If the output differs from the expected output, please consult ARRIS.
Expected Output
ssh eco@10.29.202.108
The authenticity of host '10.29.202.108 (10.29.202.108)' can't be established. RSA key fingerprint is 8e:15:49:51:3e:0a:0a:16:dd:91:e4:1b:23:c5:e1:72.

Example Results
Example/Sample output provides only a sample of what you may see as a result of an executed command. The sample should be very similar to the actual output, but may vary in terms of server name, FQDN, IP address, etc.
Example Results
ssh eco@10.29.202.108
The authenticity of host '10.29.202.108 (10.29.202.108)' can't be established. RSA key fingerprint is 8e:15:49:51:3e:0a:0a:16:dd:91:e4:1b:23:c5:e1:72.

Iterate Callout
Iterate callouts indicate that the following Section contains instructions that must be implemented on 2 or more servers. Please note that the server names are indicated as variables within the tasks you are about to perform.

The following Section contains instructions that must be implemented on multiple servers. Please note that server names may be indicated as <variables> within some tasks.

For Iterate tasks, note that any EXECUTE tasks call out the server number indicator.
EXECUTE:
ssh eco@<oltp_ecoutilXX_ip_address>

Server Tables
For Iterate steps, a table is provide denoting the specific server variable names (which also indicated the type of server). Ensure that the tasks listed in the Section are implemented on each server and in the order noted in the table.
Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

6

Database Server name
<oltp_node1_ip> <oltp_node2_ip> <oltp_node3_ip> <oltp_node4_ip> <oltp_node5_ip>

Task complete

Operator initials

Date and time

STOP Callouts
Stop callouts require the operator(s) to stop MOP execution and validate that the preceding steps were correctly implemented before proceeding to the next steps.
Please note that the task was completed, the date and time the task was completed, and the number of minutes it took.

STOP

Task Completed?


Operator Initials

Date/Time

Time to Complete

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

7

ASK YOURSELF
 Do I have the proper and the appropriate access permissions for the environment?
 Do I know why I'm doing this work?
 Have I identified and notified everybody, impacted customers and internal groups, who will be directly affected by this work?
 Can I prevent or control service interruption?
 Is this the right time to do this work?
 Am I trained and qualified to do this work?
 Are the work orders, MOPs, and supporting documentation current and errorfree?
 Do I have everything I need to quickly and safely restore service if something goes wrong?
 Have I walked through the procedure?
 Have I made sure the procedure contains proper closure, including obtaining clearance and release from the appropriate work center?

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

8

OVERVIEW
This document provides the necessary steps to enable/disable ECO Collect on ECO 4.2SR2.0.
Note: For non-production `Labs', see Appendix A for additional configuration requirements to enable ECO Collect on the Manage Servers.
Assumptions
This document assumes that the person who updates the log configuration file has a working knowledge of the Linux OS. In addition, sFTP access to the ARRIS Public sFTP server is required.
Execution of this MOP requires that team members performing the update have knowledge and experience associated with the following organizational role:
 Linux System Administrator
 ECO Application Administrator
The ECO Collect Site Preparation MOP provided by ARRIS has already been successfully executed in the environment where this MOP is to be run.
The `MOP-ATT_ECOCollect2.0-Collector_Access_Cache_Installation' MOP has already been successfully executed in the environment where this MOP is to be run.
The `MOP-ATT_ECOCollect2.0-Data_Store_Installation' MOP has already been successfully executed in the environment where this MOP is to be run.
The `MOP-ATT_ECOCollect2.0-Message_Bus_Installation' MOP has already been successfully executed in the environment where this MOP is to be run.
The `MOP-ATT_ECOCollect2.0-Collect_Load_Balancer_Guidelines' MOP has already been successfully executed in the environment where this MOP is to be run.
ECO Manage 4.2 SR2.0 has been successfully installed and is currently online in the environment where this MOP is to be run.
The `MOP-ATT_ECOCollect2.0-Collector_Event_Stream_Installation' MOP has already been successfully executed in the environment where this MOP is to be run.

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

9

The `MOP-ATT_ECOCollect2.0-Traffic_Manager_Installation' MOP has already been successfully executed in the environment where this MOP is to be run.

STOP

Task Completed?


Operator Initials

Date/Time

Time to Complete

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

10

1 PREPARATION

This Section outlines operational readiness for the configuration. This entire Section should be completed prior to entering the maintenance window. The following sequencing table provides details on the high level steps, execution times, MOP Section associated with the task, and whether the steps affect the service.

Preparation Sequencing Table
Task

MOP Approximate Role Section Duration

Service Comment Affecting

Preparation

1

Preparation Entrance

1.1

Preparation Execution

1.2

Verify the Current MOP Version 1.2.1

5 minutes

ECOAPPLICADMIN

No

Preparation Exit

1.3

Checklists completed

Summary

10 minutes

1.1 Preparation Entrance
The Preparation Entrance stage is required to verify that the system is ready for the setup. These tasks are documented in the following Sections of this document:
 Verify the Current MOP Version (Section 1.2.1)

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

11

1.2 Preparation Execution

Verify the Current MOP Version

Prior to executing this MOP, verify that it is the most current version. To verify:

1.2.1.1 Login to the ARRIS sFTP server.

o sFTP SITE: att.sftp.ps.arris.com

o DIRECTORY: /Services/att/ecocollect20/MOPs

1.2.1.2 Compare the version numbers to confirm that the MOP is the latest version.

STOP

Task Completed?


Operator Initials

Date/Time

Time to Complete

1.3 Preparation Exit
Ensure the following tasks have been completed successfully:  Verify the Current MOP Version (Section 1.2.1)

STOP

Task Completed?


Operator Initials

Date/Time

Time to Complete

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

12

2 ENABLE ECO COLLECT ON ECO MANAGE SERVERS

This Section outlines the tasks that must be performed to configure the ECO Manage Application Servers to enable ECO Collect functionality. The following sequencing table provides details of the high level steps, execution times, MOP Section associated with the task, and whether the steps affect service.

Task

MOP Approximate Role

Service

Section Duration

Affecting

Enable ECO Collect on ECO Manage Servers

2

Configuration Update Entrance 2

Configuration Update Execution 2.2 for DMC servers

Backup the Current DMC Configuration

2.2.1 5 min

ECO-APPLIC- No
ADMIN

Comment

Configure DMC to enable ECO Collect

2.2.2

5 min

ECO-APPLIC- No
ADMIN

Perform Rolling Restart of the DMC Application

2.2.3 5 min

Configuration Update Execution 2.3 for SMS Servers

5 min

Backup the Current SMS Configuration

2.3.1 5 min

Configure SMS to enable ECO Collect

2.3.2 5 min

Perform Rolling Restart of the SMS Application

2.3.3 5min

Enable ECO Collect on ECO Manage Servers Exit

2.2.3.7 5 min

ECO-APPLIC- No
ADMIN
ECO-APPLIC- No
ADMIN
ECO-APPLIC- No
ADMIN
ECO-APPLIC- No
ADMIN
ECO-APPLIC- No
ADMIN
ECO-APPLIC- No
ADMIN

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

13

Task Summary

MOP Approximate Role Section Duration
40 minutes

Service Affecting
No

Comment

2.1 Configuration Update Entrance

No additional steps are require prior to executing the configuration update.

2.2 Configuration Update Execution for DMC Servers

Owner: ECO-ADMIN

This Section covers the steps to back up the current configuration, install the updated configuration and perform a server restart to put the new configurations into action. Repeat each subsection for each ECO application server. ### Remove extra "2.2.2" entry
 Backup the Current DMC Configuration (Section 2.2.1 2.2.2)
 Configure DMC to enable ECO Collect(Section 2.2.2)
 Perform a Rolling Restart of the DMC Application (Section 2.2.3)

Backup the current ECO Manage DMC Application Server Configuration

2.2.1.1 Log into <dmcXX> ECO Manage Application Server as eco user.

2.2.1.2 Change to the /opt/pace/dmc/conf directory. Copy the .config file.

EXECUTE:

cp -p .config .config.dmc.collect.disabled

cd /opt/pace/dmc/conf cp .config .config.dmc.collect.disabled

2.2.1.3 Check that the backup file is there by performing an ls command in the directory.

EXECUTE: ls -al .config*
ls �al

### Clear this column

2.2.1.4 Ensure that these steps were performed on each dmc server.

Parameter Name
<dmc01> <dmc02>

Task Complete

Operator Initials

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

Date and Time

Date and Time
DMC Server #1 DMC Server #2

14

### Clear this column

Parameter Name
<dmc03> <dmc04> <dmc05> <dmc06> <dmc07> <dmc08> <dmc09> <dmc10> <dmc11> <dmc12>

Task Complete

Operator Initials

Date and Time

Date and Time
DMC Server #3 DMC Server #4 DMC Server #5 DMC Server #6 DMC Server #7 DMC Server #8 DMC Server #9 DMC Server #10 DMC Server #11 DMC Server #12

STOP

Task Completed?


Operator Initials

Date/Time

Time to Complete

Configure DMC to enable ECO Collect
The following section contains instructions that needs to be repeated for each applicable ECO DMC application server.
2.2.2.1 Login to ECO Manage Server as the eco user.
2.2.2.2 Change to the /opt/pace/dmc/conf directory.
EXECUTE:
cd /opt/pace/dmc/conf
2.2.2.3 Edit the .config file using vi editor:
EXECUTE:
vi .config
2.2.2.4 Edit the below section by uncommenting the 2nd line and commenting the 4th line. See below.
EXECUTE:
##Collect Enabled (Uncomment to Enable) default.dmc.Dspring.profiles.active=rabbitmq,manageServIntegration ##Collect Disabled (Uncomment to Disable) #default.dmc.Dspring.profiles.active=default

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

15

STOP

Task Completed?


Operator Initials

Date/Time

Time to Complete

2.2.2.5 Save the edited file by typing the following:
:wq!

### Clear this column

2.2.2.6 Ensure that this step is performed for each DMC application server.

Parameter Name <dmc01> <dmc02> <dmc03> <dmc04> <dmc05> <dmc06> <dmc07> <dmc08> <dmc09> <dmc10> <dmc11> <dmc12>

Task Complete

Operator Initials

Date and Time

Date and Time
DMC Server #1 DMC Server #2 DMC Server #3 DMC Server #4 DMC Server #5 DMC Server #6 DMC Server #7 DMC Server #8 DMC Server #9 DMC Server #10 DMC Server #11 DMC Server #12

STOP

Task Completed?


Operator Initials

Date/Time

Time to Complete

Perform a Rolling Restart of the DMC Application
In the previous section, there were configuration changes to the .config file. A restart of the DMC applications is required to deploy the new configuration. This section contains steps to stop and start the dmcserver application. The following steps need to be repeated for each applicable ECO application server.
NOTE: The restart should be executed ONE server at a time.
2.2.3.1 Login into each ECO Manage App Server <dmcXX> as user eco.
2.2.3.2 Stop the dmc server application.
EXECUTE:
/opt/pace/dmc/bin/dmcserver1 stop
Example Output
Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.
### Disable VIPs for current server
### Include dmc_nginx stop

16

Shutting down DMC instance server1 Shut down DMC instance server1

2.2.3.3 Wait for the application to stop and then validate that the application is not running.

EXECUTE:
ps �elf | grep /opt/pace/dmc/container/bin/bootstrap.jar
Example Output
eco ##### ##### 0 ##:## tty1 00:00:00 grep /opt/pace/dmc/container/bin/bootstrap.jar

2.2.3.4 Start the dmc server application.

EXECUTE:
/opt/pace/dmc/bin/dmcserver1 start
Example Output
Starting DMC instance server1 as eco. Started DMC instance server1

2.2.3.5 2.2.3.6

Monitor the /opt/pace/dmc/logs/dmc_server1.log log file and watch for errors

while the application is starting.

### Include after starting application:
Tail the dmc_stderr.log file to verify when the ECO Manage

Check the status of the dmc server apfoprlicDaetviiocnes. application has completely started up.

EXECUTE:

EXECUTE:

date --utc;tail -f /opt/pace/dmc/logs/dmc_stderr.log | grep -

B1 'INFO: Server startup'
/opt/pace/dmc/bin/dmcserver1 status

Example Output

Example Output: Wed Nov 13 12:12:03 UTC 2017

DMC instance server1 is rsNutonavnri1t3n,g.2017 12:16:45 AM xxx.xxxxxxx.xxxxxxx.xxxxxxx.xxxxxxx

INFO: Server startup in 135475 ms

STOP

Task Completed?


Operator Initials

Date/Time

Time to Complete

2.2.3.7 Monitor the /opt/pace/dmc/logs/dmc_server1.log log file and watch for errors while the application is starting.
2.2.3.8 Check the status of the dmc server application.
EXECUTE:
/opt/pace/dmc/bin/dmcserver1 status
Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.
### Include dmc_nginx start

17

### Enable VIPs for current server

Example Output
DMC instance server1 is running.

### Clear this column

2.2.3.9 Ensure that this step is performed on each application server.

<dmc01> ... <dmc12>

Parameter Name <sms01> <sms02> <sms03> <sms04>

Task Complete

Operator Initials

Date and Time

Date and Time
SMS Server #1 SMS Server #2 SMS Server #3 SMS Server #4

STOP

Task Completed?


Operator Initials

Date/Time

Time to Complete

2.3 Configuration Update Execution for SMS Servers

Owner: ECO-ADMIN
This section covers the steps to backup the current SMS configuration, install the updated SMS configuration and perform a server restart to put the new configuration into action. Repeat each subsection for each ECO application server.
 Backup the Current SMS Configuration (Section 2.3.1)  Configure SMS to enable ECO Collect (Section 2.3.2)  Performing a Rolling Restart of the SMS Application (Section 2.3.3 )
Backup the current ECO Manage SMS Application Configuration
2.3.1.1 Log into <smsXX> ECO Manage Application Server as eco user.

2.3.1.2 Change to the /opt/pace/sms/conf directory. Copy the .config file.

EXECUTE:

cp -p .config .config.sms.collect.disabled

cd /opt/pace/sms/conf

cp .config .config.sms.collect.disabled

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

18

2.3.1.3 Check that the backup file is there by performing an ls command in the directory.

EXECUTE:
ls -al

ls -al .config*

2.3.1.4 Ensure that these steps were performed on each SMS server.

Configure SMS to enable ECO Collect

The following section contains instructions that need to be repeated for each applicable ECO SMS application server.

2.3.2.1 Login to the ECO Manage Server as the eco user.

2.3.2.2 Change to the /opt/pace/sms/conf directory.

EXECUTE:
cd /opt/pace/sms/conf

2.3.2.3 Edit the .config using vi editor:

EXECUTE:
vi .config

2.3.2.4 Edit the below section by uncommenting the 2nd line and commenting the 4th line. See below.

EXECUTE:

##Collect Enabled (Uncomment to Enable) default.sms.Dspring.profiles.active=collect,manageServIntegration,default,rabbitmq,header Auth ##Collect Disabled (Uncomment to Disable) #default.sms.Dspring.profiles.active=default,headerAuth

2.3.2.5 Save the edited file by typing the following:
:wq!

### Clear this column

2.3.2.6 Ensure that this step is performed for each SMS application server.

Parameter Name <sms01> <sms02> <sms03>

Task Complete

Operator Initials

Date and Time

Date and Time
SMS Server #1 SMS Server #2 SMS Server #3

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

19

Parameter Name <sms04>

Task Complete

Operator Initials

Date and Time

Date and Time SMS Server #4

STOP

Task Completed?


Operator Initials

Date/Time

Time to Complete

Perform an Rolling Restart of the SMS Application
In the previous section, there was a configuration change to the .config file. A restart of the SMS application is required to deploy on the new configuration. The following steps need to be repeated for each applicable ECO application server.
NOTE: The restart should be executed ONE server at a time.
2.3.3.1 Login into each ECO Manage App Server <smsXX> as user eco.
2.3.3.2 Stop the sms server application. ### Include sms_nEgXinECxUsTtoE:p
/opt/pace/sms/bin/smsserver1 stop
Example Output
Shutting down SMS instance server1 Killing child process with PID 28537 Killing process with PID 28534 Shut down SMS instance server1
2.3.3.3 Wait for the application to stop and then validate that the application is not running.
EXECUTE:
ps �elf | grep /opt/pace/sms/container/bin/bootstrap.jar
Example Output
eco ##### ##### 0 ##:## tty1 00:00:00 grep /opt/pace/sms/container/bin/bootstrap.jar
2.3.3.4 Start the sms server application.
EXECUTE:
/opt/pace/sms/bin/smsserver1 start

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

20

### Include after starting application:
Tail the sms_server1_stderr.log file to verify when the ECO Manage for Services application has completely started up.

EXECUTE: date --utc;tail -f /opt/pace/sms/logs/sms_server1_stderr.log |
grep -B1 'INFO: Server startup'

Example Output

Example Output:

Starting SMS instance seNWroevvde1Nr3o1,v a2103s117e2c1:21o:2.1:60:34U5TACM2x0x1x7.xxxxxxx.xxxxxxx.xxxxxxx.xxxxxxx

Started SMS instance sersvtearrt1
INFO: Server startup in 135475 ms

2.3.3.5 Monitor the /opt/pace/sms/logs/sms_server1.log log file and watch for errors ### Include sms_nwghiinlextshtearatpplication is starting.

2.3.3.6 Check the status of the dmc server application.

EXECUTE:
/opt/pace/sms/bin/smsserver1 status
Example Output
SMS instance server1 is running.

### Clear this column

2.3.3.7 Ensure that this step is performed on each application server.

Parameter Name <sms01> <sms02> <sms03> <sms04>

Task Complete

Operator Initials

Date and Time

Date and Time
SMS Server #1 SMS Server #2 SMS Server #3 SMS Server #4

STOP

Task Completed?


Operator Initials

Date/Time

Time to Complete

2.4 Enable ECO Collect on ECO Manage Servers Phase Exit
Ensure that the following tasks have been completed successfully:  Backup the Current DMC Configuration (Section 2.2.1)  Configure DMC to enable ECO Collect(Section 2.2.2)  Perform a Rolling Restart of the DMC Application (Section 2.2.3)  Backup the Current SMS Configuration (Section 2.3.1)  Configure SMS to enable ECO Collect (Section 2.3.2)  Performing a Rolling Restart of the SMS Application (Section 2.3.3)

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

21

3 DISABLE ECO COLLECT ON ECO MANAGE SERVERS
This Section outlines the tasks that must be performed to configure the ECO Manage Application Servers to disable the ECO Collect functionality. The following sequencing table provides details of the high level steps, MOP Section associated with the task and whether the steps affect service.

Task

MOP Approximate Role

Service

Section Duration

Affecting

Disable ECO Collect on ECO Manage Servers

Disable ECO Collect Entrance

3.1

Disable ECO Collect Execution 3.2

Rollback from Crawl, Walk or Run 3.2.1 configuration

60 min

ECO-APPLIC- No
ADMIN

Stop DMC Application Service 3.2.2 5 min

ECO-APPLIC- No
ADMIN

Comment

Restore DMC backup file

3.2.3 5 min

ECO-APPLIC- No
ADMIN

Start DMC Application Service 3.2.4 5 min

ECO-APPLIC- No
ADMIN

Stop SMS Application Service

3.2.7 5 min

ECO-APPLIC- No
ADMIN

Restore SMS Backup file

3.2.8 5 min

ECO-APPLIC- No
ADMIN

Start SMS Application Service 3.2.9 5 min

ECO-APPLIC- No
ADMIN

Disable ECO Collect on ECO Manage Servers Phase Exit

3.3 5 min

ECO-APPLIC- No
ADMIN

Summary

100 minutes

No

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

22

3.1 Disable ECO Collect Entrance
No Additional steps are required prior to executing the configuration update.
3.2 Disable ECO Collect Execution
This section covers the steps to disable the ECO Collect functionality by rolling back the original configuration files.
 Rollback from the crawl, walk or run phase for ECO collect (Section 3.2.1)  Stop the DMC application (Section 3.2.2)  Rollback the config file on DMC Server(Section 3.2.3)  Start the DMC application(Section 3.2.4)  Stop the SMS application (Section 3.2.7 )  Rollback the config file on SMS Server (Section 3.2.8 )  Start the SMS application (Section 3.2.9 )
Use the appropriate rollback section from the appendix of the MOPATT_ECOCollect2.0-Crawl_Walk_Run_Configuration mop to rollback to original configuration.

STOP

Task Completed?


Operator Initials

Date/Time

### NOTE: The restart should be executed ONE server at a time. Stop the DMC server by performing the below steps.
EXECUTE:
/opt/pace/dmc/bin/dmcserver1 stop
Example Output
Shutting down DMC instance server1 Shut down DMC instance server1

### Disable VIPs for current server ### Include dmc_nginx stop

Time to Complete

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

23

### Include copy cmd to save "enabled" settings for future use

Re-install the original .config file on the dmc application as eco user. Choose `y' to overwrite the file when pccroppm--pppte..dcctooonndffoiiggs.od.. cmocn.cfoigll.edcmtc..dcoilsalebclted.e.nacoblnefdig

Backup "enabled" configuration and...

EXECUTE:
cd /opt/pace/dmc/conf

Execute: ls -al .config*

cp .config.dmc.collect.disabled .config

Start the DMC server by performing the below steps.

EXECUTE:

/opt/pace/dmc/bin/dmcserver1 start

Example Output

### Include after starting application:

Tail the dmc_stderr.log file to verify when the ECO Manage Starting DMC instance server1 as ecfoor Devices application has completely started up. Started DMC instance server1

EXECUTE:
Check the status of the DMC server bdyapteer-f-ourtmc;intagitlhe-fb/eoloptw/psatceep/sd.mc/logs/dmc_stderr.log | grep -
B1 'INFO: Server startup'

EXECUTE:

Example Output:

/opt/pace/dmc/bin/dmcserver1 statusWed Nov 13 12:12:03 UTC 2017 Nov 13, 2017 12:16:45 AM xxx.xxxxxxx.xxxxxxx.xxxxxxx.xxxxxxx

Example Output

start INFO: Server startup in 135475 ms

DMC instance server1 is running

Ensure these steps are done each DMC server.

Parameter Name <dmc01> <dmc02> <dmc03> <dmc04> <dmc05> <dmc06> <dmc07> <dmc08> <dmc09> <dmc10> <dmc11> <dmc12>

Task Complete

Operator Initials

Date and Time

Date and Time
DMC Server #1 DMC Server #2 DMC Server #3 DMC Server #4 DMC Server #5 DMC Server #6 DMC Server #7 DMC Server #8 DMC Server #9 DMC Server #10 DMC Server #11 DMC Server #12

STOP

Task Completed?


Operator Initials

Date/Time

Time to Complete

### Include dmc_nginx start
### Enable VIPs for current server
Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

24

### Include sms_nginx stop

Stop the SMS server by performing the below steps.

EXECUTE:
/opt/pace/sms/bin/smsserver1 stop

Example Output

Shutting down SMS instance server1 Shut down SMS instance server1
### Include copy cmd to save "enabled" settings for future use

Re-install the original .config file. Choose `y' to overwrite the file when

prompted to do so.

cp -p .config .config.sms.collect.enabled cp -p .config.sms.collect.disabled .config

Backup "enabled"

EXECUTE:

configuration and... cd /opt/pace/sms/conf

Execute: ls -al .config*

cp .config.sms.collect.disabled .config

Start the SMS server by performing the below steps.

EXECUTE:

/opt/pace/sms/bin/smsserver1 start

Example Output

### Include after starting application:

Starting SMS instance server1 asTaeiclo the sms_server1_stderr.log file to verify when the ECO

Started SMS instance server1

Manage for Services application has completely started up.

Check the status of the SMS serverdEbaXEtyeCpU-Te-Eru:ftocr;mtainilg t-hfe/bopetl/opwacset/espmss/. logs/sms_server1_stderr.log |

EXECUTE:

grep -B1 'INFO: Server startup'

Example Output:

/opt/pace/sms/bin/smsserver1 staWteuds Nov 13 12:12:03 UTC 2017

Nov 13, 2017 12:16:45 AM xxx.xxxxxxx.xxxxxxx.xxxxxxx.xxxxxxx

Example Output

start

INFO: Server startup in 135475 ms

SMS instance server1 is running

### Include sms_nginx start

Ensure that these steps are performed on each sms server.

Parameter Name <sms01> <sms02> <sms03> <sms04>

Task Complete

Operator Initials

Date and Time

Date and Time
SMS Server #1 SMS Server #2 SMS Server #3 SMS Server #4

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

25

3.3 Disable ECO Collect on ECO Manage Servers Phase exit
 Rollback from the crawl, walk or run phase for ECO collect (Section 3.2.1)  Stop the DMC application (Section 3.2.2)  Rollback the config file on DMC Server(Section 3.2.3)  Start the DMC application(Section 3.2.4)  Stop the SMS application (Section 3.2.7 )  Rollback the config file on SMS Server (Section 3.2.8 )  Start the SMS application (Section 3.2.9 )
### Insert page-break

APPENDIX A: NON PRODUCTION LAB CONFIGURATION REQUIREMENTS TO ENABLE ECO COLLECT.

For a lab environment, the DMC installation.properties file has to be

modified to include the configuration for ECO collect. The SMS

installation.properties file also has to be modified to include

configuration for ECO Collect.

### Fix filename: /opt/pace/dmc/conf/installation.properties

A.1.1 Edit the /opt/pace/dmc/conf/Installation. Properties file to include the

host name of all the servers that have RabbitMQ installed for your specific lab

environment. See the below example.

!!!Example only!!!
############################################ ##############ECO COLLECT################### ############################################

# #RabbitMQ Config - Comma separated list of RabbitMQ servers, user and password created in RabbitMQ # rabbit.addresses=hpa60c1,hpa60c2,hpa60c3,hpa60c4,hpa60c5,hpa60c6 rabbit.user=collect
Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

26

rabbit.password=m2bEspAQuxuZ rabbit.vhost=/
rabbit.exchange=traffic-manager-message-exchange

A.1.2 Edit the /opt/pace/sms/conf/installation.properties file to include the

Cassandra contact points, RabbitMQ and collect url in the configuration file. See

below example.

### Remove Cassandra section from MOP; Does not exist within SMS

installation.properties

!!!Example only!!!

####################ECO COLLECT######################## #######################################################

# Enable/disable historical diagnostics - Needs to BE False for enabling ECO Collect # sms.service.historicalDiagnostics.disable=false

#Uncomment the following lines to enable ECO Collect # # Cassandra Parameters # cassandra.keyspace=eco_collect_att cassandra.cluster=Test Cluster cassandra.replicationFactor=3 cassandra.maxConnectionsPerHost=15 cassandra.maxColumnLimitOnRowQueries=100 cassandra.contactPoints=hpa60a-app

# #RabbitMQ Config - Comma separated list of RabbitMQ servers, user and password created in RabbitMQ # rabbit.addresses=hpa60c1,hpa60c2,hpa60c3,hpa60c4,hpa60c4,hpa60c5,hpa 60c6 rabbit.user=collect rabbit.password=m2bEspAQuxuZ rabbit.vhost=/
# #ECO Collect Endpoint Properties # sms.collect.username=collect sms.collect.password=m2bEspAQuxuZ

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

27

sms.collect.url=http://collect-int.hpa60.ps.2wire.com sms.collect.connectionTimeout=5000 sms.collect.requestTimeout=20000 sms.collect.readTimeout=60000 sms.collect.idleConnectionTimeout=60000 #

Copyright 2017 � ARRIS Enterprises Inc. All rights reserved.

28

